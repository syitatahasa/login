<?php
require 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">
	<title>Login</title>
</head>

<body>


	<div class="login">
		<p>LOGIN</p>
		<br />
		<form method="post">
			<div>
				<label>Username:</label>
				<input type="text" name="username" placeholder="nama khusus" required />
			</div>
			<div>
				<label>Password:</label>
				<input type="password" name="pwd" placeholder="Masukkan Password" required />
			</div>
			<div>
				<button name="login" type="submit" style="background-color: #e3f2fd;">Login</button>
			</div>
			<br>
			<div class="small"><a href="register.php">Need an account? Sign up!</a></div>

		</form>
		<br>
		<h5 class="identitas">
			=Syi'ta Al Mar'atush Sholihah - V3420073=
		</h5>
	</div>

</body>

</html>